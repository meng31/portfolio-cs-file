﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;

public class StateMachine<T>
{
    public Istate<T> CurState { get; protected set; }
        
    private T m_sender;

    public StateMachine(T sender, Istate<T> state)
    {
        m_sender = sender;
        Setstate(state);
    }



    public void Setstate(Istate<T> state)
    {
        if (m_sender == null)
        {
            Debug.LogError("센더 없당");
            return;
        }

        if (CurState == state)
        {
            Debug.LogWarningFormat("Already Define State - {0}", state);
            return;
        }

        if (CurState != null)
        {
            CurState.OnExit(m_sender);
        }

        CurState = state;

        if (CurState != null)
        {
            CurState.OnEnter(m_sender);
        }
    }

    public void OnFixedUpdate()
    {
        if (m_sender == null)
        {
            Debug.LogError("invalid m_sener");
            return;
        }
        CurState.OnFixedUpdate(m_sender);
    }

    public void OnUpdate()
    {
        if (m_sender == null)
        {
            Debug.LogError("센더가 안보영");
            return;
        }
        CurState.OnUpdate(m_sender);
    }
}
