﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;
using Player;
public class SkillManager : Singleton<SkillManager>
{
   
    public bool isArcaneMeteor { get; set; }
    public bool isSpinAttack { get; set; }
    public bool isFlyAtk { get; set; }

    public static bool isEquip;
    public bool changeOK { get; set; }
    public bool _PlayerEffectCheck { get; set; }
    public bool isBuffOn;
    WaitForSeconds SpinCoolTime = new WaitForSeconds(5f);
    WaitForSeconds FlyCoolTime = new WaitForSeconds(6f);
    WaitForSeconds ArcaneCoolTime = new WaitForSeconds(10f);
    WaitForSeconds BuffDurationTime = new WaitForSeconds(20f);
    Coroutine BuffCoroutine;

    private void Awake()
    {
        isEquip = false;
        changeOK = true;
        
    }


    public void SkillManage()
    {
        
        switch (Input.inputString)
        {
            case "1":
                if (!isSpinAttack && isEquip && changeOK)
                {
                    PlayerController.Instance.ChangeState(PlayerController.PState.SpinAttack);
                    isSpinAttack = true;
                    StartCoroutine(CoolTimeManager(PlayerController.PState.SpinAttack));
                }
                break;
            case "2":
                if (!isFlyAtk && isEquip && changeOK)
                {
                    PlayerController.Instance.ChangeState(PlayerController.PState.FlyAttack);
                    isFlyAtk = true;
                    StartCoroutine(CoolTimeManager(PlayerController.PState.FlyAttack));
                }
                break;
            case "3":
                if (!isArcaneMeteor && !isEquip && changeOK)
                {
                    isArcaneMeteor = true;
                    PlayerController.Instance.ChangeState(PlayerController.PState.ArcaneMeteor);
                    StartCoroutine(CoolTimeManager(PlayerController.PState.ArcaneMeteor));
                }
                break;
        }

    }

    public void NormalAttack()
    {
        if (Input.GetMouseButtonDown(0) && changeOK && isEquip)
        {
            PlayerController.Instance.ChangeState(PlayerController.PState.AttackCombo);
        }
    }

    public void EquipSword()
    {
        if (changeOK)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                changeOK = false;
                if(isEquip && isBuffOn)
                {
                    PlayerController.Instance.SwordPointLight.SetActive(false);
                    PlayerController.Instance.SwordEffect.GetComponent<PSMeshRendererUpdater>().IsActive = false;
                    PlayerController.Instance.SwordEffect.transform.GetChild(0).gameObject.SetActive(false);
                    isBuffOn = false;
                    StopCoroutine(BuffCoroutine);
                }

                PlayerMovement.Instance.EquipAnimation(ref isEquip);

            }
        }
        else
        {
            if (PlayerMovement.Instance.m_animator.GetCurrentAnimatorStateInfo(1).IsName("None"))
            {
                changeOK = true;
            }
        }
    }

    IEnumerator CoolTimeManager(PlayerController.PState SkillName)
    {
        switch (SkillName)
        {
            case PlayerController.PState.SpinAttack:
                yield return SpinCoolTime;
                isSpinAttack = false;
                break;
            case PlayerController.PState.FlyAttack:
                yield return FlyCoolTime;
                isFlyAtk = false;
                break;
            case PlayerController.PState.ArcaneMeteor:
                yield return ArcaneCoolTime;
                isArcaneMeteor = false;
                break;
        }

    }

    public void BuffOn()
    {
        if(!isBuffOn)
        {
            if(Input.GetKeyDown(KeyCode.T) && changeOK && isEquip)
            {
                isBuffOn = true;
                PlayerController.Instance.ChangeState(PlayerController.PState.Buff);
                BuffCoroutine = StartCoroutine(Buff());
            }
        }
    }

    IEnumerator Buff()
    {
        yield return BuffDurationTime;
        isBuffOn = false;
        PlayerController.Instance.SwordPointLight.SetActive(false);
        PlayerController.Instance.SwordEffect.GetComponent<PSMeshRendererUpdater>().IsActive = false;
        PlayerController.Instance.SwordEffect.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void OnEffect(string skillname, Transform target, Quaternion rotation, float despawnTime, bool isparents = false)
    {
        
        GameObject go = objectPollingManager.Instance.Spawn(skillname, target.position, rotation);

        if (isparents)
            go.transform.parent = target;

        if (go != null)
        {
             objectPollingManager.Instance.Despawn(go, despawnTime);
        }
      
    }

}
