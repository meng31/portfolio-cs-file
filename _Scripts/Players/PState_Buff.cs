using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_Buff : Istate<PlayerController>
    {
        bool isEnd;
        bool firstTime;

        public PState_Buff()
        {
            firstTime = true;
        }

        public void OnEnter(PlayerController player)
        {
            isEnd = false;
            AnimationManager.Instance.changeAnimationTrigger(player.m_Animator, "Buff", ref player.curAniState);
        }


        public void OnUpdate(PlayerController player)
        {
            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Buff") &&
             player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.4f)
            {
                if (firstTime)
                {
                    player.SwordEffect.GetComponent<PSMeshRendererUpdater>().UpdateMeshEffect();
                    player.SwordEffect.SetActive(true);
                    player.SwordPointLight.SetActive(true);
                    player.SwordEffect.transform.GetChild(0).gameObject.SetActive(true);
                    
                    firstTime = false;
                    isEnd = true;
                }
                else if(!isEnd)
                {
                    player.SwordEffect.GetComponent<PSMeshRendererUpdater>().IsActive = true;
                    player.SwordPointLight.SetActive(true);
                    player.SwordEffect.transform.GetChild(0).gameObject.SetActive(true);
                    isEnd = true;
                }
                
            }

            if(AnimationManager.Instance.isEndAni(player.m_Animator,"Buff"))
            {
                player.ChangeState(PlayerController.PState.Idle);
            }
        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
        }


    }
}