using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_AttackCombo3 : Istate<PlayerController>
    {

        public void OnEnter(PlayerController player)
        {
            player.WeaponFx.SetActive(true);
            player.m_Animator.SetTrigger("Attack");
            player.EffectCheck = false;
        }


        public void OnUpdate(PlayerController player)
        {
            PlayerMovement.Instance.preventBurstGravity();

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Combo3"))
            {

                if (!player.EffectCheck)
                {
                    if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.33f)
                    {
                        player.EffectCheck = true;
                        player.AttackCollision.SetActive(true);
                    }
                }



                if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.5f)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        player.ChangeState(PlayerController.PState.AttackCombo4);
                        return;
                    }
                }



                if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                {
                    player.ChangeState(PlayerController.PState.Idle);
                }
            }


        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
            player.WeaponFx.SetActive(false);
        }


    }
}