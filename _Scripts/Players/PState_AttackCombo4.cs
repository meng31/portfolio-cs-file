using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_AttackCombo4 : Istate<PlayerController>
    {
        bool AttackBoxOn;

        public void OnEnter(PlayerController player)
        {
            player.WeaponFx.SetActive(true);
            player.m_Animator.SetTrigger("Attack");
            player.EffectCheck = false;
            AttackBoxOn = false;
        }


        public void OnUpdate(PlayerController player)
        {
            PlayerMovement.Instance.preventBurstGravity();

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Combo4"))
            {
                if (!AttackBoxOn)
                {
                    if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.15f)
                    {
                        AttackBoxOn = true;
                        player.AttackCollision.SetActive(true);
                    }
                }


                if (!player.EffectCheck)
                {
                        player.EffectCheck = true;
                        SkillManager.Instance.OnEffect("Ascension", player.AscensionPosition, player.AscensionPosition.rotation, 7f);
                }


                    if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                {
                    player.ChangeState(PlayerController.PState.Idle);
                }
            }

        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
            player.WeaponFx.SetActive(false);
        }


    }
}