using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{


	public class PlayerMovement : Singleton<PlayerMovement>
	{
		[HideInInspector]
		public Animator m_animator;
		[HideInInspector]
		public Rigidbody m_RigidBody;
		[HideInInspector]
		public Vector3 m_GroundNormal;


		public GameObject Sword;
		public float slopeForce;

		float timeCheck = 0f; 
		float m_TurnAmount;
		float m_ForwardAmount;
		float m_StationaryTurnSpeed = 180f;
		float m_MovingTurnSpeed = 360f;
		float animationTurn;
		float m_GroundCheckDistance = 0.5f;

		private void Awake()
		{
			m_animator = PlayerController.Instance.m_Animator;
			m_RigidBody = GetComponent<Rigidbody>();
		}

		public void Move(Vector3 move)
		{
			if (move.magnitude > 1f) move.Normalize();
			move = transform.InverseTransformDirection(move);
			GroundNormalCheck();
			move = Vector3.ProjectOnPlane(move, m_GroundNormal);
			m_TurnAmount = Mathf.Atan2(move.x, move.z);
			animationTurn = m_TurnAmount * Mathf.Rad2Deg / 180f;
			m_ForwardAmount = move.z;

			if (isSlopeCheck())
			{
				m_RigidBody.useGravity = false;
			}
			else
			{
				m_RigidBody.useGravity = true;
			}


			if (isSlopeCheck() && move.magnitude > 0f)
			{
				m_RigidBody.AddForce(-m_GroundNormal * slopeForce);
			}

			ApplyTurnRotation();
			UpdateAnimator();
		}

		void ApplyTurnRotation()
		{
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}


		void UpdateAnimator()
		{
			m_animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_animator.SetFloat("Turn", animationTurn, 0.2f, Time.deltaTime);
		}

		public void ResetFloat()
		{
			m_animator.SetFloat("Forward", 0f);
			m_animator.SetFloat("Turn", 0f);
		}

		public void EquipAnimation(ref bool isEquip)
		{
			if (!isEquip)
			{
				m_animator.SetTrigger("Equip");
				isEquip = true;
			}
			else
			{
				m_animator.SetTrigger("UnEquip");
				isEquip = false;
			}
		}

		bool isSlopeCheck()
		{
			RaycastHit hitInfo;

			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{
				if (hitInfo.normal != Vector3.up)
					return true;
			}
			return false;
		}

		void GroundNormalCheck()
		{
			RaycastHit hitinfo;

			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitinfo, 0.5f))
			{
				m_GroundNormal = hitinfo.normal;

			}
			else
            {
				m_GroundNormal = Vector3.up;
            }
		}

		public void preventBurstGravity(float weight = 16f)
        {
            GroundNormalCheck();
            m_RigidBody.AddForce(-m_GroundNormal * weight);
        }



		IEnumerator DownForce(float time, float weight)
		{
			while (true)
			{
				m_RigidBody.AddForce(-m_GroundNormal * weight);
				timeCheck += Time.deltaTime;
				if (timeCheck > time)
				{
					timeCheck = 0f;

					if (PState_Idle.isIdle) ResetFloat();

					break;

				}
				yield return null;
			}
		}

		public void _SlopeDownForce(float time, float weight)
		{
			if (isSlopeCheck())
			{

				StartCoroutine(DownForce(time, weight));
				print("����ȴ�");
			}
		}

		

		public void equip()
		{
			Sword.gameObject.SetActive(true);
		}

		public void unequip()
		{
			Sword.gameObject.SetActive(false);
		}


	}
}