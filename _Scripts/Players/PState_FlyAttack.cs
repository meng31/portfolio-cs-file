using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;

namespace Player
{
    public class PState_FlyAttack : Istate<PlayerController>
    {

        public void OnEnter(PlayerController player)
        {
            AnimationManager.Instance.changeAnimationTrigger(player.m_Animator, "FlyAttack", ref player.curAniState);
            player.EffectCheck = false;
            player.WeaponFx.SetActive(true);
            
        }


        public void OnUpdate(PlayerController player)
        {

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("FlyAttack"))
            {

                if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.4f)
                    PlayerMovement.Instance.preventBurstGravity(100f);

                if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.41f)
                {

                    if (!player.EffectCheck)
                    {
                        SkillManager.Instance.OnEffect("FullSwing", player.FlyAttackPosition, player.FlyAttackPosition.rotation, 0.8f);
                        player.EffectCheck = true;
                    }
                }

                if (player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                    player.ChangeState(PlayerController.PState.Idle);
            }
        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
            player.WeaponFx.SetActive(false);
        }


    }
}