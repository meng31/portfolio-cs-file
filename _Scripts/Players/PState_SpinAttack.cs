using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_SpinAttack : Istate<PlayerController>
    {

        public void OnEnter(PlayerController player)
        {
            AnimationManager.Instance.changeAnimationTrigger(player.m_Animator, "SpinAttack", ref player.curAniState);
            player.EffectCheck = false;
            player.WeaponFx.SetActive(true);
        }


        public void OnUpdate(PlayerController player)
        {
            PlayerMovement.Instance.preventBurstGravity();

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("SpinAttack") &&
            player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.46f)
            {
                if (!player.EffectCheck)
                {
                    SkillManager.Instance.OnEffect("FlyAttack", player.SpinAttackPosition, player.SpinAttackPosition.rotation, 1.5f);
                    CameraShake.Instance._CameraShake(0.5f, 1f, 10, 60, false, true);
                    player.EffectCheck = true;
                }
            }

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("SpinAttack") &&
            player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                player.ChangeState(PlayerController.PState.Idle);

        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
            player.WeaponFx.SetActive(false);
        }


    }
}