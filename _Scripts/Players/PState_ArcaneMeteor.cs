using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_ArcaneMeteor : Istate<PlayerController>
    {

        public void OnEnter(PlayerController player)
        {
            AnimationManager.Instance.changeAnimationTrigger(player.m_Animator, "ArcaneMeteor", ref player.curAniState);
            player.EffectCheck = false;
            SkillManager.Instance.OnEffect("HandEffect", player.HandEffectPosition, player.HandEffectPosition.rotation, 2.6f, true);
            
        }


        public void OnUpdate(PlayerController player)
        {
            PlayerMovement.Instance.preventBurstGravity();

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("ArcaneMeteor") &&
            player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.46f)
            {
                if (!player.EffectCheck)
                {
                    SkillManager.Instance.OnEffect("ArcaneMeteor", player.ArcaneMeteorPosition, player.ArcaneMeteorPosition.rotation, 7f);
                    player.EffectCheck = true;
                }
            }

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("ArcaneMeteor") &&
            player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                player.ChangeState(PlayerController.PState.Idle);

        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
            player.WeaponFx.SetActive(false);
        }


    }
}