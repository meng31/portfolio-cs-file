using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{

    public class PState_Idle : Istate<PlayerController>
    {
        public static bool isIdle;
        float h_input;
        float v_input;

        public void OnEnter(PlayerController player)
        {
            isIdle = true;
            AnimationManager.Instance.changeAnimationTrigger(player.m_Animator, "onMovement", ref player.curAniState);
            
            PlayerMovement.Instance._SlopeDownForce(0.25f, 100f); // 경사로 미끄러짐 현상과 붕뜸 현상을 방지하는 해결책.
            
        }
        public void OnUpdate(PlayerController player)
        {
            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Movement"))
            {
                // idle과 move의 구별을위해 idle상태에서도 input값을 넣어서 받아놓는다.
                h_input = Input.GetAxis("Horizontal");
                v_input = Input.GetAxis("Vertical");
                PlayerMovement.Instance.Move(Vector3.zero);  // 자연스러운 움직임을위해 멈춰있음에도, 무브먼트에 move코드를 실행시켜서 idle진입시 자연스럽게 멈추는것처럼 보이게 만든다.


                if (Mathf.Abs(h_input) > 0.1f || Mathf.Abs(v_input) > 0.1f) // input값을 받아놓고있으니 절대값을통해 상하좌우 어떤입력값이 들어오기만해도 바로 move로 넘어가게끔 만든다.
                    player.ChangeState(PlayerController.PState.Move);

              
             

                SkillManager.Instance.NormalAttack();
                SkillManager.Instance.SkillManage();
                SkillManager.Instance.EquipSword();
                SkillManager.Instance.BuffOn();
            }
        }
        public void OnFixedUpdate(PlayerController player)
        {
        }
        public void OnExit(PlayerController player)
        {
            PlayerMovement.Instance.ResetFloat(); // idle상태에서 공격하거나 행동후에 다시 idle로 돌아왔을때 블렌드트리의 파라미터값을 0,0으로 초기화시켜줘야 캐릭터가 이상한행동을 하지않는다.
            isIdle = false;
        }


    }

}