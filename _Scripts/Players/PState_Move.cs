using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_Move : Istate<PlayerController>
    {
        private Transform m_Cam;
        private Vector3 m_camForward;
        private Vector3 m_Move;
        float h_input;
        float v_input;
       
       
        private bool ToIdle;

        public PState_Move()
        {
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning("메인카메라가 없습니다.");
            }
            
        }

        public void OnEnter(PlayerController player)
        {
          
            ToIdle = false;
        }
        public void OnUpdate(PlayerController player)
        {
            h_input = Input.GetAxis("Horizontal");
            v_input = Input.GetAxis("Vertical");

            if (m_Cam != null)
            {
                m_camForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v_input * m_camForward + h_input * m_Cam.right;
            }
            else
            {
                Debug.LogError("메인카메라 없음");
                return;
            }

           


            PlayerMovement.Instance.Move(m_Move);

            SkillManager.Instance.NormalAttack();
            SkillManager.Instance.SkillManage();
            SkillManager.Instance.EquipSword();
            SkillManager.Instance.BuffOn();


            if (Mathf.Abs(h_input) < 0.1f && Mathf.Abs(v_input) < 0.1f)
            {
                ToIdle = true;
                player.ChangeState(PlayerController.PState.Idle);
            }

        }

        public void OnFixedUpdate(PlayerController player)
        {
        }

        public void OnExit(PlayerController player)
        {
            if(!ToIdle)
            {
                PlayerMovement.Instance.ResetFloat();
            }
            
            
        }

    }
}
