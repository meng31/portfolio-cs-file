using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{


    public class PlayerController : Singleton<PlayerController>
    {
        public enum PState
        {
            Idle,
            Move,
            AttackCombo,
            AttackCombo2,
            AttackCombo3,
            AttackCombo4,
            Buff,
            FlyAttack,
            SpinAttack,
            ArcaneMeteor,
            KnockDown
         
        }

        private StateMachine<PlayerController> PStateMachine;
        private Dictionary<PState, Istate<PlayerController>> states = new Dictionary<PState, Istate<PlayerController>>();

        [HideInInspector]
        public Animator m_Animator;
        [HideInInspector]
        public string curAniState;

        public GameObject SwordPointLight;
        public GameObject SwordEffect;
        public GameObject WeaponFx;
        public GameObject AttackCollision;
        public Transform FlyAttackPosition;
        public Transform SpinAttackPosition;
        public Transform HandEffectPosition;
        public Transform ArcaneMeteorPosition;
        public Transform AscensionPosition;

        [HideInInspector]
        public float inStateTime;
        [HideInInspector]
        public bool EffectCheck;

        private int playerHP;
        private float invincibleTime;
        private bool isInvincible;

        private void Awake()
        {
            m_Animator = this.GetComponent<Animator>();
            playerHP = 100;
            invincibleTime = 0f;
            isInvincible = false;
        }

        private void Start()
        {
            states.Add(PState.Idle, new PState_Idle());
            states.Add(PState.Move, new PState_Move());
            states.Add(PState.Buff, new PState_Buff());
            states.Add(PState.AttackCombo, new PState_AttackCombo());
            states.Add(PState.AttackCombo2, new PState_AttackCombo2());
            states.Add(PState.AttackCombo3, new PState_AttackCombo3());
            states.Add(PState.AttackCombo4, new PState_AttackCombo4());
            states.Add(PState.FlyAttack, new PState_FlyAttack());
            states.Add(PState.SpinAttack, new PState_SpinAttack());
            states.Add(PState.ArcaneMeteor, new PState_ArcaneMeteor());
            states.Add(PState.KnockDown, new PState_KnockDown());
            


            PStateMachine = new StateMachine<PlayerController>(this, states[PState.Idle]);
        }

        private void FixedUpdate()
        {
            PStateMachine.OnFixedUpdate();
        }

        private void Update()
        {
            PStateMachine.OnUpdate();

            if(isInvincible)
            {
                invincibleTime += Time.deltaTime;
                if(invincibleTime > 0.6f)
                {
                    isInvincible = false;
                    invincibleTime = 0f;
                }
            }
        }

        public void ChangeState(PState state)
        {
            Debug.LogWarning(state);
            PStateMachine.Setstate(states[state]);
        }

        public void GetHit(int damage)
        {
            if (isInvincible)
                return;

            playerHP -= damage;
            CameraShake.Instance._CameraShake(0.4f, 0.5f, 5, 45, false, true);

            isInvincible = true;

            if (SkillManager.Instance.changeOK)
            {
                if (PState_Idle.isIdle)
                    m_Animator.SetTrigger("IdleHit");
                else
                    m_Animator.SetTrigger("GetHit");
            }
        }

        public void GetKnockDown(int damage)
        {
            Debug.Log("����");
            playerHP -= damage;
            ChangeState(PState.KnockDown);
        }

    }
}
