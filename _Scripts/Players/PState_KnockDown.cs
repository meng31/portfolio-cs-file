using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PState_KnockDown : Istate<PlayerController>
    {

        public void OnEnter(PlayerController player)
        {
            AnimationManager.Instance.changeAnimationTrigger(player.m_Animator, "KnockDown", ref player.curAniState);
           
        }

        public void OnUpdate(PlayerController player)
        {
            PlayerMovement.Instance.preventBurstGravity();

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("KnockDown") &&
            player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.5f)
            {
                PlayerMovement.Instance.m_RigidBody.AddForce(-player.transform.forward * 200f);
            }

            if (player.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Rise") &&
            player.m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.91f)
            {
                player.ChangeState(PlayerController.PState.Idle);
            }
        }


        public void OnFixedUpdate(PlayerController player)
        {
        }


        public void OnExit(PlayerController player)
        {
            player.WeaponFx.SetActive(false);
        }


    }
}
