using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
	public class Camera_Controller : MonoBehaviour
	{
		public Transform target;
		float distance;

		[SerializeField] private float Sensitivity = 0.1f;//감도
		[SerializeField] private float yminLimit = -5f;// 위아래 회전제한
		[SerializeField] private float yMaxiLimit = 50f;

		[SerializeField] private float zoomSpeed = 2f;
		[SerializeField] private float maxDistance = 30f;
		[SerializeField] private float minDistance = 10f;

		float x, y;

		private void Awake()
		{
			Vector3 angles = this.transform.eulerAngles;
			x = angles.y;
			y = angles.x;
			distance = Vector3.Distance(target.position, this.transform.position);
		}



		private void Update()
		{


			if (Input.GetMouseButton(1))
				RotationCamera();

			cameraZoom();
		}

		private void LateUpdate()
		{
			if (!target)
				return;

			transform.position = transform.rotation * new Vector3(0, 0, -distance) + target.position;
		}

		private void RotationCamera()
		{
			y -= Input.GetAxis("Mouse Y") * Sensitivity * Time.deltaTime;
			x += Input.GetAxis("Mouse X") * Sensitivity * Time.deltaTime;

			y = clampAngle(y, yminLimit, yMaxiLimit);
			this.transform.rotation = Quaternion.Euler(y, x, 0);


		}

		private void cameraZoom()
		{
			distance -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed * Time.deltaTime;
			distance = Mathf.Clamp(distance, minDistance, maxDistance);

		}

		float clampAngle(float angle, float min, float max)
		{
			if (angle < -360) { angle += 360; }
			if (angle > 360) { angle -= 360; }

			return Mathf.Clamp(angle, min, max);
		}


	}
}

