using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;

public class DragonAttackCollider2 : MonoBehaviour
{
    Collider _collider;
    float timecheck;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }

    void OnEnable()
    {
        _collider.enabled = false;
        timecheck = 0f;
    }

    private void Update()
    {
        timecheck += Time.deltaTime;

        if (timecheck > 0.3f)
            _collider.enabled = false;
        else if (timecheck > 0.1f)
            _collider.enabled = true;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _collider.enabled = false;
            PlayerController.Instance.GetKnockDown(10);
        }
    }
}
