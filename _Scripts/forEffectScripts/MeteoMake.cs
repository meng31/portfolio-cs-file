﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;
public class MeteoMake : _ObjectMakeBase
{

    public Vector3 m_randomRotationValue;


    void OnEnable()
    {


        GameObject m_obj = objectPollingManager.Instance.Spawn("Meteo", transform.position, transform.rotation);
        m_obj.transform.parent = this.transform;
        m_obj.transform.rotation *= Quaternion.Euler(GetRandomVector(m_randomRotationValue));

        if(m_obj != null)
        {
            objectPollingManager.Instance.Despawn(m_obj, 1f);
        }

        if (m_movePos)
        {
            if (m_obj.GetComponent<MoveToObject>())
            {
                MoveToObject m_script = m_obj.GetComponent<MoveToObject>();
                m_script.m_movePos = m_movePos;
            }
        }

    }
}

