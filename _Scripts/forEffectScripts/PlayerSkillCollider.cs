﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkillCollider : MonoBehaviour
{
    Collider _collider;
    float disappear;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        
    }

    private void OnEnable()
    {
        _collider.enabled = true;
        disappear = 0;
    }

    private void Update()
    {
        disappear += Time.deltaTime;

        if (disappear > 0.3f)
            _collider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Dragon"))
        {
            CameraShake.Instance._CameraShake(0.5f, 1f, 10, 60, false, true);
            _collider.enabled = false;
            EnemyCtrl.Instance.getHit(10);
        }
    }

}
