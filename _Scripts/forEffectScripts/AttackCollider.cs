﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCollider : MonoBehaviour
{
    Collider _collider;
    float timeCheck;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        
    }

    private void OnEnable()
    {
        timeCheck = 0f;
        _collider.enabled = true;
    }


    private void Update()
    {

        timeCheck += Time.deltaTime;

        if (timeCheck > 0.2f)
        {
            _collider.enabled = false;
            this.gameObject.SetActive(false);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dragon"))
        {
            _collider.enabled = false;
            EnemyCtrl.Instance.getHit(5);
            CameraShake.Instance._CameraShake(0.2f, 0.5f, 8, 45, false, true);
        }
    }

}
