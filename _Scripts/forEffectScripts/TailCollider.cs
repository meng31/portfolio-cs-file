﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;

public class TailCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().GetHit(5);
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if(other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().GetHit(5);
        }
    }

}
