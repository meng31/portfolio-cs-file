﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToMeteo : MonoBehaviour
{
    public Transform m_movePos;
    public float m_startDelay;
    public float m_durationTime;
    public float m_lerpValue;
    public float m_lookValue;
    public Transform resetTrans;
    public bool isok;
    float m_Time;

    void Start()
    {
        m_Time = Time.time;
    }

    private void OnEnable()
    {
        m_Time = Time.time;
        this.transform.position = resetTrans.position;
        isok = false;
    }

    void Update()
    {
        if (Time.time > m_Time + m_startDelay)
        {
            if (Time.time < m_Time + m_durationTime + m_startDelay)
            {
                transform.position = Vector3.Lerp(transform.position, m_movePos.position, Time.deltaTime * m_lerpValue);
                if (Vector3.Distance(transform.position, m_movePos.position) > 0.5f)
                {
                   
                    
                    Quaternion lookPos = Quaternion.LookRotation(transform.position - m_movePos.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, (lookPos), Time.deltaTime * m_lookValue);
                }
                else
                {
                    if (!isok)
                    {
                        CameraShake.Instance._CameraShake(0.2f, 0.8f, 10, 90f, false, false);
                        isok = true;
                    }
                }
            }
        }
    }
}
