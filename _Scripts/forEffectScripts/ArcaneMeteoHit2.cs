using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneMeteoHit2 : MonoBehaviour
{
    Collider _collider;
    float inTime;
    bool isHit;

    private void Awake()
    {
        _collider = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        inTime = 0f;
        _collider.enabled = false;
        isHit = false;
    }

    private void Update()
    {
        inTime += Time.deltaTime;

        if(inTime >= 2.0f)
        {
            if(!isHit)
            {
                _collider.enabled = true;
                CameraShake.Instance._CameraShake(1f, 1.2f, 15, 80, false, true);
                isHit = true;
            }
        }

        if(inTime > 2.3f)
        {
            _collider.enabled = false;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dragon"))
        {
            if (other.CompareTag("Dragon"))
            {
                _collider.enabled = false;
                EnemyCtrl.Instance.getHit(10);
            }
        }

    }
}
