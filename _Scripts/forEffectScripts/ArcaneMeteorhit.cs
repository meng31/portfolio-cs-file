using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneMeteorhit : MonoBehaviour
{
    Collider _collider;

    private void Awake()
    {
        _collider = GetComponent<CapsuleCollider>();
    }

    private void OnEnable()
    {
        _collider.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Dragon"))
        {
            if (other.CompareTag("Dragon"))
            {
                _collider.enabled = false;
                EnemyCtrl.Instance.getHit(10);
            }
        }

    }
}
