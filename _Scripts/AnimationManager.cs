﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : Singleton<AnimationManager>
{



    public bool isEndAni(Animator animator, string CurrentAni)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(CurrentAni) &&
            animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
        {
            return true;
        }

        return false;
    }

    public void changeAnimationTrigger(Animator animi, string newState, ref string CurState) //애니메이션 스테이트를 바꿔주는 함수
    {
        if (CurState == newState)
        {
            //Debug.Log("이미 상태가" + newState + "입니다.");
            return;  // 트리거는 단 한번만 호출되도록
        }

        animi.SetTrigger(newState);
        CurState = newState;
        Debug.Log("현재 애니메이션 상태는" + CurState);
    }
}
