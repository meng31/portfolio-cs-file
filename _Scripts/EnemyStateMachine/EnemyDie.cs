﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDie : Istate<EnemyCtrl>
{

    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0;
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onDie", ref enemy.currentAniState);
    }

    public void OnExit(EnemyCtrl enemy)
    {

    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        enemy._inStateTime += Time.deltaTime;

        if (enemy._inStateTime > 10f)
        {
            enemy.gameObject.SetActive(false);
        }

    }

}
