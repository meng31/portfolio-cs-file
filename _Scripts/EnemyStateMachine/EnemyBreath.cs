﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;


public class EnemyBreath : Istate<EnemyCtrl>
{
    private float BreathRotateSpeed = 3f;
    private float effectStart = 1.9f;
    GameObject BreathEffect;
    
    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        enemy.EffectCheck = false;
        EnemyMovement.Instance.stopMoving();
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator,"onBreath", ref enemy.currentAniState);
       
    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 3f;
        EnemyAttackPattern.Instance.AttackPatternCheck();
        objectPollingManager.Instance.Despawn(BreathEffect);
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {
        
    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        enemy._inStateTime += Time.deltaTime;


        EnemyMovement.Instance.Turn();
        if (enemy._inStateTime >1f)
            EnemyMovement.Instance.DoRotation(BreathRotateSpeed);

        if (enemy._inStateTime > effectStart)
        {
            if (!enemy.EffectCheck)
            {
                BreathEffect = objectPollingManager.Instance.Spawn("DragonBreath", enemy.BreathPoint.position, enemy.BreathPoint.rotation);
                BreathEffect.transform.parent = enemy.BreathPoint;
                enemy.EffectCheck = true;
            }
        }
        

        if(AnimationManager.Instance.isEndAni(enemy.animator,"Breath"))
        {
            enemy.ChangeState(EnemyCtrl.eState.Wait);
        }

    }

}
