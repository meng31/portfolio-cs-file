﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWait : Istate<EnemyCtrl>
{
    float _reAttackTime = 4.2f;

    public void OnEnter(EnemyCtrl enemy)
    {
     
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onWait", ref enemy.currentAniState);
    }

    public void OnExit(EnemyCtrl enemy)
    {
        
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {

        EnemyAttackPattern.Instance._WaitTime += Time.deltaTime;
       

        if (EnemyAttackPattern.Instance._WaitTime > _reAttackTime)
        {
            if (EnemyAttackPattern.Instance._GoStun)
            {
                enemy.ChangeState(EnemyCtrl.eState.Stun);
            }
            else
            {
                enemy.ChangeState(EnemyCtrl.eState.goAttack);
            }
        }
            

    }

}
