﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;
using UnityEngine.AI;


public class EnemyCtrl : Singleton<EnemyCtrl>
{
    public enum eState
    {
        Idle, 
        Patrol,
        Trace,
        LookAround,
        goAttack,
        Breath,
        Spit,
        TailSwing,
        Firerain,
        Wait,
        Groggy,
        FlyingAttack,
        Stun,
        Die
    }
        
    private StateMachine<EnemyCtrl> m_DragonStateMachine;
    private Dictionary<eState, Istate<EnemyCtrl>> m_states = new Dictionary<eState, Istate<EnemyCtrl>>();

    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public NavMeshAgent m_agent;

    
    public Transform[] _patrolPoint;
    public Transform Player;
    public Transform BreathPoint; //불뿜는위치
    public Transform SpitPoint;  // 머리박는 이펙트나오는 위치
    public Transform EarthQuakePoint;  //어스퀘이크 스킬이펙트위치
    public Transform SteamEffectPoint; // 입김 이펙트 위치
    public GameObject TailSwing; // 꼬리치기할때만 트리커이벤트를 실행하기위해 그순간에만 오브젝트를 켜서 트리커체크 가능하도록 만듬

    [HideInInspector]
    public string currentAniState; // 애니메이션 전환시 중복호출이 안되게하는 변수
    [HideInInspector]
    public bool EffectCheck; // 각스테이트의 업데이트문에서 이펙트호출시 이펙트를 호출하자마자 바로 false로 만들어서 한번만 호출하게 하는 변수.
    [HideInInspector]
    public int _dragonHP;
    [HideInInspector]
    public float _inStateTime; //스테이트 안에서 흘러간 시간을 체크해주는 거의 모든 스테이트에서 쓰는 프로퍼티 
    private bool isDragonDie; // 용이 죽었는지 체크
    private bool GroggyisOnlyOneTime;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        m_agent = GetComponent<NavMeshAgent>();
        _dragonHP = 100;
        isDragonDie = false;
        GroggyisOnlyOneTime = false;
    }

    private void Start()
    {
        m_states.Add(eState.Idle, new EnemyIdle());
        m_states.Add(eState.Trace, new EnemyTrace());
        m_states.Add(eState.Patrol, new EnemyPatrol());
        m_states.Add(eState.LookAround, new EnemyLookAround());
        m_states.Add(eState.goAttack, new EnemyGoAttack());
        m_states.Add(eState.Breath, new EnemyBreath());
        m_states.Add(eState.Spit, new EnemySpit());
        m_states.Add(eState.TailSwing, new EnemyTailSwing());
        m_states.Add(eState.Firerain, new EnemyFirerain());
        m_states.Add(eState.Wait, new EnemyWait());
        m_states.Add(eState.Groggy, new EnemyGroggy());
        m_states.Add(eState.FlyingAttack, new EnemyFlyingAttack());
        m_states.Add(eState.Stun, new EnemyStun());
        m_states.Add(eState.Die, new EnemyDie());

        m_DragonStateMachine = new StateMachine<EnemyCtrl>(this, m_states[eState.Idle]);

    }

    private void Update()
    {
        m_DragonStateMachine.OnUpdate();
        
        if (!isDragonDie)
        {
            if (_dragonHP < 0)
            {
                isDragonDie = true;
                ChangeState(eState.Die);
            }
        }
    }

    private void FixedUpdate()
    {
        m_DragonStateMachine.OnFixedUpdate();

    }

    public void ChangeState(eState state)
    {
        Debug.LogWarning("현재 스테이트 = " + state);
        m_DragonStateMachine.Setstate(m_states[state]);
    }

    public void getHit(int damage) //플레이어한테 스킬로 맞았을 때
    {
        _dragonHP -= damage; // 일단 맞으면 무조건 피는 깎여야해

        if (!GroggyisOnlyOneTime)
        {
            if (_dragonHP <= 50)
            {
                GroggyisOnlyOneTime = true;
                ChangeState(eState.Groggy);
                return;
            }
        }

        animator.SetTrigger("onHit");
    }
}
