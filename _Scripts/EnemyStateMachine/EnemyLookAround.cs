﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLookAround : Istate<EnemyCtrl>
{
    float rotateTime = 4f;
    float endLookAroundTime = 5f;
    float lookRotSpeed = 2f;
    float SearchDistance = 60f;

    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onLook", ref enemy.currentAniState);
    }

    public void OnExit(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {
        
    }

    public void OnUpdate(EnemyCtrl enemy)
    {

        enemy._inStateTime += Time.deltaTime;


        Quaternion q1 = enemy.transform.rotation;
        Quaternion q2 = Quaternion.LookRotation(Vector3.back);


        if (enemy._inStateTime < rotateTime)
        { enemy.transform.rotation = Quaternion.Lerp(q1, q2, lookRotSpeed * Time.deltaTime); }

        if (enemy._inStateTime > endLookAroundTime)
        {
            enemy.ChangeState(EnemyCtrl.eState.Patrol);
        }


        if (EnemyMovement.Instance.SerchTarget(SearchDistance))
            enemy.ChangeState(EnemyCtrl.eState.Trace);
    }

    
}
