﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : Singleton<EnemyMovement>
{
    
    private Transform Target;  
    private Transform dragonBody;
    float _attackRotateSpeed = 5f;

    void Awake()
    {
        Target = EnemyCtrl.Instance.Player;
        dragonBody = EnemyCtrl.Instance.transform;
    }


    public void doMoveNavMesh()
    {
        
        EnemyCtrl.Instance.m_agent.SetDestination(Target.position); 
        //타겟의 포지션으로 navmesh를 통해서 이동한다.
        
    }

    public void stopMoving()
    {
        EnemyCtrl.Instance.m_agent.ResetPath();  
        // 타겟이 공격상태에 돌입하면 그 스테이트의 onEnter에서 이 메써드를 호출해서 더이상 이동하지않게한다.
    }

    public void DoRotation(float rotSpeed)
    {
        Vector3 dir = Target.position - base.transform.position;
        dir.y = 0;
        Quaternion q1 = base.transform.rotation;
        Quaternion q2 = Quaternion.LookRotation(dir.normalized);
        base.transform.rotation = Quaternion.Slerp(q1, q2, rotSpeed * Time.deltaTime);
        
    }

    public bool SerchTarget(float searchDistance) 
    {
        RaycastHit hit;
        Vector3 dir = Target.position - dragonBody.position;
        dir.y = 0;
        Debug.DrawRay(dragonBody.position, dir.normalized, Color.blue, searchDistance);
        if (Physics.Raycast(transform.position + Vector3.up * 1f, dir.normalized, out hit, searchDistance))
        {
            if (hit.collider.tag.Equals("Player"))
            {
                return true;
            }
        }
        return false;
    } 

    public void Turn()
    {
        if (EnemyCtrl.Instance._inStateTime < 1f)
        {
            DoRotation(_attackRotateSpeed);
        }
    }



    //회전, 이동, 적 체크하는 함수들 (최소 2개의 스테이트에서 공유하는) 을 Movement스크립트에서 관리하고있습니다.

}
