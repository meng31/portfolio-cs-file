﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdle : Istate<EnemyCtrl>
{
    
    
    public void OnEnter(EnemyCtrl enemy)
    {
        
    }

    public void OnExit(EnemyCtrl enemy)
    {

    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            enemy.ChangeState(EnemyCtrl.eState.Patrol);
        }
    }

}
