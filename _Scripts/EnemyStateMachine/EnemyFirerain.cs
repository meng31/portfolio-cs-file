﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;

public class EnemyFirerain : Istate<EnemyCtrl>
{

    private float _FirerainStart = 1.3f;
   

    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        enemy.EffectCheck = false;
        EnemyMovement.Instance.stopMoving();
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator,"onFirerain", ref enemy.currentAniState);
        //SkillManager.Instance.DelayEffect(_FirerainStart, "FireRain", enemy.Player, Quaternion.Euler(-90, 0, 0), 7.2f);
    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 0f;
        EnemyAttackPattern.Instance.AttackPatternCheck();
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        enemy._inStateTime += Time.deltaTime;
        EnemyMovement.Instance.Turn();
        if (enemy._inStateTime > _FirerainStart)
        {
            if (!enemy.EffectCheck)
            {
                SkillManager.Instance.OnEffect("FireRain", enemy.Player, Quaternion.Euler(-90, 0, 0), 7.5f);
                //TestDoTween.Instance.CameraShake(7f);
                enemy.EffectCheck = true;

            }
        }
       

        if (AnimationManager.Instance.isEndAni(enemy.animator, "Firerain"))
        {
            enemy.ChangeState(EnemyCtrl.eState.Wait);
        }
       
    }

}