﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroggy : MonoBehaviour, Istate<EnemyCtrl> 
{

    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onGroggy", ref enemy.currentAniState);
    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 3f;
        EnemyAttackPattern.Instance._AttackIndex = 0;
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        enemy._inStateTime += Time.deltaTime;

        if (enemy._inStateTime > 7f)
        {
            AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onReturn", ref enemy.currentAniState);
        }

        if(AnimationManager.Instance.isEndAni(enemy.animator, "Return"))
        { 
            enemy.ChangeState(EnemyCtrl.eState.Wait);
        }
    }

}
