﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : Istate<EnemyCtrl>
{
    private int patrolCount; // 총 패트롤 지점
    private float patrolSpeed = 6f; // 패트롤시 속도
    private int currentPatrolPointIndex = 0; //현재 몇번째 패트롤지점인지
    float movingRotSpeed = 3f;
    float SearchDistance = 60f;

    public void OnEnter(EnemyCtrl enemy)
    {
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onPatrol", ref enemy.currentAniState);
        patrolCount = enemy._patrolPoint.Length;
    }

    public void OnExit(EnemyCtrl enemy)
    {

    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        
        Vector3 patrolPoint = enemy._patrolPoint[currentPatrolPointIndex].position;
        Vector3 dir = patrolPoint - enemy.transform.position;
        dir.y = 0f;
        enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, patrolPoint, patrolSpeed * Time.deltaTime);
        Quaternion q1 = enemy.transform.rotation;
        Quaternion q2 = Quaternion.LookRotation(dir.normalized);


        enemy.transform.rotation = Quaternion.Lerp(q1, q2, movingRotSpeed * Time.deltaTime);

        if (Vector3.Distance(enemy.transform.position, patrolPoint) < 0.3f)
        {
            countUp();
            enemy.ChangeState(EnemyCtrl.eState.LookAround);
        }


        if (EnemyMovement.Instance.SerchTarget(SearchDistance))
            enemy.ChangeState(EnemyCtrl.eState.Trace);
    }

    public void countUp()
    {
        if (currentPatrolPointIndex < patrolCount - 1)
        {
            currentPatrolPointIndex++;
        }
        else
        {
            currentPatrolPointIndex = 0;
        }
    }


}

