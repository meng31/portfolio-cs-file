﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;

public class EnemySpit : Istate<EnemyCtrl>
{

    private float _SpitStart = 1.7f;
    

    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        enemy.EffectCheck = false;
        EnemyMovement.Instance.stopMoving();
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator,"onSpit", ref enemy.currentAniState);
        SkillManager.Instance.OnEffect("SteamEffect", enemy.SteamEffectPoint, Quaternion.identity, 2f, true);

    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 2.5f;
        EnemyAttackPattern.Instance.AttackPatternCheck();
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        enemy._inStateTime += Time.deltaTime;
        EnemyMovement.Instance.Turn();
        if (enemy._inStateTime > _SpitStart)
        {
            if (!enemy.EffectCheck)
            {
                SkillManager.Instance.OnEffect("FireBall", enemy.SpitPoint, enemy.SpitPoint.rotation, 5f);
                CameraShake.Instance._CameraShake(0.2f, 1f, 10, 90, false, true);
                enemy.EffectCheck = true;
            }
        }

        if(AnimationManager.Instance.isEndAni(enemy.animator, "Spit"))
        {
            enemy.ChangeState(EnemyCtrl.eState.Wait);
        }

    }

}
