﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;

public class EnemyAttackPattern : Singleton<EnemyAttackPattern>
{
    public string[] attackPattern;
    public int _AttackPatternSize { get; set; }
    public int _AttackIndex { get; set; }
    public float _WaitTime { get; set;} // 드래곤이 공격하고나서 바로공격하지않고 기다리는 시간.
    public bool _GoStun { get; set; }

    

    void Awake()
    {
        _AttackIndex = 0;
    }

    private List<string> PickCommands(string str) 
    {
        List<string> list = new List<string>();
        string[] splits = str.Split(','); 
        // 만약 매개인자로 받은 string str의 값이 A,B,C 였다면
        // splits라는 스트링배열에 현재 들어가있는 요소는 splits[0] = A , splits[1] = B, splits[2] = C 가 된다.
        foreach (var split in splits)
            list.Add(split);

        return list;
    }

    public List<string> ChooseRandomPattern()
    {
        if (attackPattern.Length == 0)
            return null;

        int id = Random.Range(0, attackPattern.Length);
        List<string> commands = PickCommands(attackPattern[id]);

        return commands;
        
    }

  
    public void AttackPatternCheck()
    {
        if (_AttackIndex < _AttackPatternSize - 1)
        {
            _AttackIndex++;
        }
        else
        {
            _AttackIndex = 0;
            _GoStun = true;
        }
    }
}
