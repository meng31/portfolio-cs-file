﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTailSwing : Istate<EnemyCtrl>
{
    float _SwingStart = 0.4f;


    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        EnemyMovement.Instance.stopMoving();
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onTailSwing", ref enemy.currentAniState);
    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 3f;
        EnemyAttackPattern.Instance.AttackPatternCheck();
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {

        
        enemy._inStateTime += Time.deltaTime;

        EnemyMovement.Instance.Turn();

        if(enemy._inStateTime > _SwingStart)
        {
            enemy.TailSwing.SetActive(true);
        }

        if(enemy._inStateTime > 1.5f)
        {
            enemy.TailSwing.SetActive(false);
        }

        if(AnimationManager.Instance.isEndAni(enemy.animator, "TailSwing"))
        {
            enemy.ChangeState(EnemyCtrl.eState.Wait);
        }
    }

}
