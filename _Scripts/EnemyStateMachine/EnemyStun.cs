﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStun : Istate<EnemyCtrl>
{
    
    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator,"onStun", ref enemy.currentAniState);
        EnemyAttackPattern.Instance._GoStun = false;
    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 3.7f;
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        
        enemy._inStateTime += Time.deltaTime;
        if (enemy._inStateTime > 6f)
            enemy.ChangeState(EnemyCtrl.eState.Wait);

    }


}
