﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using jongmyeong;

public class EnemyFlyingAttack : Istate<EnemyCtrl>
{
    
   
    float flyingSpeed = 30f;
    float downSpeed = 60f;
    
    Vector3 revisePosition;


    public void OnEnter(EnemyCtrl enemy)
    {
        enemy._inStateTime = 0f;
        enemy.EffectCheck = false;
        EnemyMovement.Instance.stopMoving();
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onFlyingAttack", ref enemy.currentAniState);
        enemy.m_agent.enabled = false; // 플라잉효과를 주기위해 잠시 네브매쉬에이전트를 비활성화한다.
        
    }

    public void OnExit(EnemyCtrl enemy)
    {
        EnemyAttackPattern.Instance._WaitTime = 0f;
        EnemyAttackPattern.Instance.AttackPatternCheck();
        enemy.m_agent.enabled = true; // 다시 착지했으니 다시 활성화해준다.
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        
        enemy._inStateTime += Time.deltaTime;
        EnemyMovement.Instance.Turn();

        if (enemy._inStateTime > 0.25f && enemy._inStateTime < 1.5f)
        {
            enemy.transform.Translate(Vector3.up * flyingSpeed * Time.deltaTime);
        }


        if (enemy._inStateTime > 1.5f )
        {
            AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onDown", ref enemy.currentAniState);
            enemy.transform.Translate(Vector3.down * downSpeed * Time.deltaTime);

            if (enemy.transform.position.y < 0.1f)
            {
                revisePosition = enemy.transform.position;
                revisePosition.y = 0f;
                enemy.transform.position = revisePosition;
                AnimationManager.Instance.changeAnimationTrigger(enemy.animator, "onLanding", ref enemy.currentAniState);
                enemy.ChangeState(EnemyCtrl.eState.Wait);

                if (!enemy.EffectCheck)
                {
                    SkillManager.Instance.OnEffect("Earthquake", enemy.EarthQuakePoint, enemy.EarthQuakePoint.rotation, 3f, true);
                    CameraShake.Instance._CameraShake(1.5f, 1.5f, 30, 90, false, true);
                    enemy.EffectCheck = true;
                }
            }
        }

    }
   
}
