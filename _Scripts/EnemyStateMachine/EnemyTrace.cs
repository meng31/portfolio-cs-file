﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTrace : Istate<EnemyCtrl>
{
    float changeDistance = 40f;

    public void OnEnter(EnemyCtrl enemy)
    {
        AnimationManager.Instance.changeAnimationTrigger(enemy.animator,"onTrace", ref enemy.currentAniState);
    }

    public void OnExit(EnemyCtrl enemy)
    {
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {
    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        
        EnemyMovement.Instance.doMoveNavMesh();
        

        if (EnemyMovement.Instance.SerchTarget(changeDistance))
           enemy.ChangeState(EnemyCtrl.eState.goAttack);
    }

}

