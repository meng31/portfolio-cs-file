﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGoAttack : Istate<EnemyCtrl>
{
    List<string> commands;


    public void OnEnter(EnemyCtrl enemy)
    {
        if (EnemyAttackPattern.Instance._AttackIndex == 0) // 어택 인덱스가 0일때만 새로운 패턴을 선택한다.
        { commands = EnemyAttackPattern.Instance.ChooseRandomPattern(); }

        EnemyAttackPattern.Instance._AttackPatternSize = commands.Count; // 어택패턴의 사이즈는 선택한 패턴값의 크기로 결정한다.
    }

    public void OnExit(EnemyCtrl enemy)
    {
        
    }

    public void OnFixedUpdate(EnemyCtrl enemy)
    {

    }

    public void OnUpdate(EnemyCtrl enemy)
    {
        ChangeState(enemy, commands[EnemyAttackPattern.Instance._AttackIndex]);
    }

    void ChangeState(EnemyCtrl enemy, string command)
    {
        switch (command)
        {
            case "B":
                ApproachAttackDistance(enemy, EnemyCtrl.eState.Breath, 20f);
                break;
            case "F":
                ApproachAttackDistance(enemy, EnemyCtrl.eState.FlyingAttack, 13f);
                break;
            case "R":
                ApproachAttackDistance(enemy, EnemyCtrl.eState.Firerain, 25f);
                break;
            case "S":
                ApproachAttackDistance(enemy, EnemyCtrl.eState.Spit, 13f);
                break;
            case "T":
                ApproachAttackDistance(enemy, EnemyCtrl.eState.TailSwing, 18f);
                break;
        }
    }

    void ApproachAttackDistance(EnemyCtrl enemy, EnemyCtrl.eState state, float AttackDistance)
    {
        if (!EnemyMovement.Instance.SerchTarget(AttackDistance))
        {
            AnimationManager.Instance.changeAnimationTrigger(enemy.animator,"onTrace", ref enemy.currentAniState);
            EnemyMovement.Instance.doMoveNavMesh();
        }
        else
        {
            enemy.ChangeState(state);
        }

    }

}
