﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraShake : Singleton<CameraShake>
{

    public void _CameraShake(float length, float strength, int vibrato, float randomness, bool snapping, bool fadeout)
    {
        StartCoroutine(_CameraShake_(length, strength, vibrato, randomness, snapping, fadeout));
    }

    IEnumerator _CameraShake_(float length, float strength, int vibrato, float randomness, bool snapping, bool fadeout)
    {
        transform.DOShakePosition(length, strength, vibrato, randomness, snapping, fadeout);
        yield return new WaitForSeconds(length);
        this.transform.localPosition = new Vector3(0, 1.44f, 0);
    }


}
